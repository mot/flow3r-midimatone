# MidiMatone

Otamatone app supercharged with MIDI powers.

## Usage

![The setup](./midimatron.jpg)

Connect your MIDI source to the right TRS jack of the flow3r. If you have a Launchpad Pro Mk3, you can just use the cable that came with the flow3r. MIDI devices with a DIN jack will require a DIN-TRS adapter to be used (untested).

The pin config is currently hardcoded for Type A (Data on tip). You can change the assignment in `MIDI.__init__` if needed.

No USB-MIDI yet, didn't have time to look into this.

## Future extensions

Random things that I'd like to have but probably will never build in:

* Have the note velocity do something
* MIDI CC messages to set parameters (maybe pitch wonkyness)
* Configure a MIDI channel, so it can be used properly in a longer MIDI chain
